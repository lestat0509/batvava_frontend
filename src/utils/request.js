import Vue from 'vue'
import axios from 'axios'
import { VueAxios } from './axios'
import _this from '../main.js'
import router from '../router.js'

// 创建 axios 实例
const service = axios.create({
  baseURL: '/api/admin', // api base_url
  timeout: 6000 // 请求超时时间
})

const err = (error) => {
  if (error.response){
    console.log(error.response.status)
    if (error.response.status=== 401){
      router.push({path: '/'});
    }

  }
  return Promise.reject(error)
};

// request 拦截器
service.interceptors.request.use(config => {
  const token = window.localStorage.getItem('token')
  if (token) {
    config.headers[ 'Authorization' ] = 'Bearer' + ' ' + token // 让每个请求携带自定义 token 请根据实际情况自行修改
  }
  return config
}, err)

// response 拦截器
service.interceptors.response.use((response) => {
    return response.data
  }, err)

const installer = {
  vm: {},
  install (Vue, router = {}) {
    Vue.use(VueAxios, router, service)
  }
}

export {
  installer as VueAxios,
  service as axios
}
