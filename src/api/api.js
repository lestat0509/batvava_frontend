import { axios } from '@/utils/request'

export function login (params) {
  return axios({
    url: '/auth/login',
    method: 'post',
    data: params
  })
}

export function changePassword (params) {
  return axios({
    url: '/change_password',
    method: 'put',
    data: params
  })
}

export function getUser (params) {
  return axios({
    url: '/users',
    method: 'get',
    params: params
  })
}

export function recharge (params) {
  return axios({
    url: '/recharge',
    method: 'post',
    data: params
  })
}

export function getOrderList (params) {
  return axios({
    url: '/branch_orders',
    method: 'get',
    params: params
  })
}

export function handleOrders (id,params) {
  return axios({
    url: '/orders/' + id + '/check',
    method: 'put',
    data: params
  })
}
